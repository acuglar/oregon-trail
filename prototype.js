function Traveler(name) {
    this.name = name;
    this.food = 1;
    this.isHealthy = true;
}

Traveler.prototype = {
    constructor: Traveler,
    hunt: function () {
        return this.food += 2
    },
    eat: function () {
        if (this.food) {
            return this.food--
        } else {
            return this.isHealthy = false
        }
    }
}


function Wagon(capacity) {
    this.capacity = capacity
    this.passengers = []
}

Wagon.prototype = {
    constructor: Wagon,
    getAvailableSeatCount: function () {
        return this.capacity - this.passengers.length
    },
    join: function (traveler) {
        if (this.capacity > this.passengers.length) {
            return this.passengers.push(traveler)
        }
    },
    shouldQuarantine: function () {
        let quarantine = (someone) => someone.isHealthy === false;
        return this.passengers.some(quarantine)  //true if someone.isHealthy === false
    },
    totalFood: function () {
        let totalFood = 0
        for (let element of this.passengers) {
            totalFood += element.food
        }
        return totalFood
    }
}



// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);